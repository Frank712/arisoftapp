package app.arisoft_app.Modelo;

public class ExistenciaActual {
    private String idalmacen;
    private String descripcion;
    private String codigoProducto1;
    private String codigoProducto2;
    private String existenciaActual;
    private String serie;
    private String codigo;
    private String almacen;
    private String Fventa;
    private String items;

    public String getItems() {
        return items;
    }

    public void setItems(String items) {
        this.items = items;
    }

    public String getFventa() {
        return Fventa;
    }

    public void setFventa(String fventa) {
        Fventa = fventa;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getAlmacen() {
        return almacen;
    }

    public void setAlmacen(String almacen) {
        this.almacen = almacen;
    }

    public String getSerie() {
        return serie;
    }

    public void setSerie(String serie) {
        this.serie = serie;
    }

    public String getIdalmacen() {
        return idalmacen;
    }

    public void setIdalmacen(String idalmacen) {
        this.idalmacen = idalmacen;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getCodigoProducto1() {
        return codigoProducto1;
    }

    public void setCodigoProducto1(String codigoProducto1) {
        this.codigoProducto1 = codigoProducto1;
    }

    public String getCodigoProducto2() {
        return codigoProducto2;
    }

    public void setCodigoProducto2(String codigoProducto2) {
        this.codigoProducto2 = codigoProducto2;
    }

    public String getExistenciaActual() {
        return existenciaActual;
    }

    public void setExistenciaActual(String existenciaActual) {
        this.existenciaActual = existenciaActual;
    }
}
