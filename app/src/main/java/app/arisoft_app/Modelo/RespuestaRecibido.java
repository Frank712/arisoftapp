package app.arisoft_app.Modelo;

import java.util.ArrayList;

public class RespuestaRecibido {
    private ArrayList<RecibidoWS> recibido;

    public ArrayList<RecibidoWS> getRecibido() {
        return recibido;
    }

    public void setRecibido(ArrayList<RecibidoWS> recibido) {
        this.recibido = recibido;
    }
}
